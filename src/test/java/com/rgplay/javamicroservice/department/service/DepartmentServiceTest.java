package com.rgplay.javamicroservice.department.service;

import com.rgplay.javamicroservice.department.entity.Department;
import com.rgplay.javamicroservice.department.repository.DepartmentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DepartmentServiceTest {

    @Autowired
    private DepartmentService departmentService;

    @MockBean
    private DepartmentRepository departmentRepository;

    @BeforeEach
    void setUp() {
        Department department = Department.builder().
                departmentName("InformationTechnology").
                departmentAddress("Hyderabad").
                departmentCode("IT").
                build();
        String departmentName = "InformationTechnology";
        Mockito.when(departmentRepository.findByDepartmentName(departmentName)).thenReturn(department);

    }

    @Test
    @DisplayName("Get data based on valid Department Name")
    public void whenValidDepartmentName_thenDepartmentShouldFound() {
        String departmentName = "InformationTechnology";
        Department found = departmentService.findDepartmentByName(departmentName);
        assertEquals(found.getDepartmentName(),departmentName);
   }
}