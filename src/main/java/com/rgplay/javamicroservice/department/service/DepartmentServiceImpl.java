package com.rgplay.javamicroservice.department.service;

import com.rgplay.javamicroservice.department.entity.Department;
import com.rgplay.javamicroservice.department.exceptions.DepartmentNotFoundException;
import com.rgplay.javamicroservice.department.repository.DepartmentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Slf4j
public class DepartmentServiceImpl implements DepartmentService{

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public Department saveDepartment(Department department){
        log.info("Inside saveDepartment of DepartmentService");
        return departmentRepository.save(department);
    }

    @Override
    public Department findDepartmentById(Long departmentId) throws DepartmentNotFoundException {
        log.info("Inside findDepartmentById of DepartmentService");
        Optional<Department> department = departmentRepository.findById(departmentId);

        if (!department.isPresent()){
            throw new DepartmentNotFoundException("Department Not Found!!");
        }
        return department.get();
    }

    @Override
    public List<Department> findAllDepartments() {
        log.info("Inside findAll of DepartmentService");
        return departmentRepository.findAll();
    }

    @Override
    public void deleteDepartmentById(Long departmentId) {
        log.info("Inside deleteDepartmentById of DepartmentService");
        departmentRepository.deleteById(departmentId);
    }

    @Override
    public Department updateDepartment(Long departmentId, Department department) {
        Department departmentToUpdate = departmentRepository.findById(departmentId).get();
        if(Objects.nonNull(department.getDepartmentName()) && ! "".equalsIgnoreCase(department.getDepartmentName())){
            departmentToUpdate.setDepartmentName(department.getDepartmentName());
        }

        if(Objects.nonNull(department.getDepartmentCode()) && ! "".equalsIgnoreCase(department.getDepartmentCode())){
            departmentToUpdate.setDepartmentCode(department.getDepartmentCode());
        }

        if(Objects.nonNull(department.getDepartmentAddress()) && ! "".equalsIgnoreCase(department.getDepartmentAddress())){
            departmentToUpdate.setDepartmentAddress(department.getDepartmentAddress());
        }
        departmentRepository.save(departmentToUpdate);
        return departmentToUpdate;
    }

    @Override
    public Department findDepartmentByName(String departmentName) {
        log.info("Inside findDepartmfindDepartmentByNameentById of DepartmentService");
        return departmentRepository.findByDepartmentName(departmentName);
    }
}
