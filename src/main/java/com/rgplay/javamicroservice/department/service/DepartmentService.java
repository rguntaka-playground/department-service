package com.rgplay.javamicroservice.department.service;

import com.rgplay.javamicroservice.department.entity.Department;
import com.rgplay.javamicroservice.department.exceptions.DepartmentNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;


public interface DepartmentService {

    public Department saveDepartment(Department department);

    public Department findDepartmentById(Long departmentId) throws DepartmentNotFoundException;

    public List<Department> findAllDepartments();

    public void deleteDepartmentById(Long departmentId);

    public Department updateDepartment(Long departmentId, Department department);

    public Department findDepartmentByName(String departmentName);
}
