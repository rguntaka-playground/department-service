package com.rgplay.javamicroservice.department.controller;

import com.rgplay.javamicroservice.department.entity.Department;
import com.rgplay.javamicroservice.department.exceptions.DepartmentNotFoundException;
import com.rgplay.javamicroservice.department.service.DepartmentService;
import com.rgplay.javamicroservice.department.service.DepartmentServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/departments")
@Slf4j
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;


    @PostMapping("/")
    public Department saveDepartment(@Valid @RequestBody Department department) { //@Valid will validate the incoming data as defined in @Entity @NotBlank
        log.info("Inside saveDepartment of DepartmentController");
        return departmentService.saveDepartment(department);
    }


    @GetMapping("/")
    public List<Department> findAllDepartments() {
        log.info("Inside findAllDepartments of DepartmentController");
        return departmentService.findAllDepartments();
    }

    @GetMapping("/{id}")
    public Department findDepartmentById(@PathVariable("id") Long departmentId) throws DepartmentNotFoundException {
        log.info("Inside findDepartmentById of DepartmentController");
        return departmentService.findDepartmentById(departmentId);
    }

    @GetMapping("/name/{name}")
    public Department findDepartmentByName(@PathVariable("name") String departmentName){
        log.info("Inside findDepartmentByName of DepartmentController");
        return departmentService.findDepartmentByName(departmentName);
    }

    @DeleteMapping("/{id}")
    public void deleteDepartmentById(@PathVariable("id") Long departmentId){
        departmentService.deleteDepartmentById(departmentId);
    }

    @PutMapping("/{id}")
    public Department updateDepartment(@PathVariable("id") Long departmentId, @RequestBody Department department){
        return departmentService.updateDepartment(departmentId, department);
    }

}
